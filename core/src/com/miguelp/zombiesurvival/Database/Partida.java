package com.miguelp.zombiesurvival.Database;

/**
 * Created by empimcf on 15/03/2018.
 */

public class Partida {

    private int id;
    private int zombiesMatados;
    private int zombiesTotales;

    public Partida(int zombiesMatados, int zombiesTotales){
        this.zombiesMatados = zombiesMatados;
        this.zombiesTotales = zombiesTotales;
    }

    public Partida(int id, int zombiesMatados, int zombiesTotales){
        this(zombiesMatados, zombiesTotales);
        this.id = id;
    }


    public int getZombiesMatados(){
        return zombiesMatados;
    }

    public int getZombiesTotales(){
        return zombiesTotales;
    }

    public int getZombiesEscapados(){
        return zombiesTotales-zombiesMatados;
    }

    public int getId(){
        return id;
    }

    @Override
    public String toString(){
        return getZombiesMatados() +" / " +getZombiesTotales();
    }
}
