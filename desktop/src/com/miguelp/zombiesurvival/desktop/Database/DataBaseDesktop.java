package com.miguelp.zombiesurvival.desktop.Database;

import com.miguelp.zombiesurvival.Database.Partida;
import com.miguelp.zombiesurvival.Database.ZombieDataBase;
import java.util.ArrayList;

/**
 * Created by Miguel on 31/01/2018.
 * CUIDADO: IMPLEMENTACIÓN DUMMY
 */

public class DataBaseDesktop implements ZombieDataBase {

    @Override
    public void borrarPeorPartida() {

    }

    @Override
    public ArrayList<Partida> getListaPartidas() {
        ArrayList<Partida> partidas = new ArrayList<Partida>();
        partidas.add(new Partida(1,2,5));
        partidas.add(new Partida(2,5,6));
        partidas.add(new Partida(3,4,2));
        return partidas;
    }

    @Override
    public void guardarPartida(Partida partida) {

    }

    @Override
    public boolean esTop3(Partida partida) {
        return false;
    }

}
