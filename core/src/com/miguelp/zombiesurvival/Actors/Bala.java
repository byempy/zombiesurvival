package com.miguelp.zombiesurvival.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.miguelp.zombiesurvival.*;
import com.miguelp.zombiesurvival.Pantallas.Pantalla;

/**
 * Created by empimcf on 13/03/2018.
 */

public class Bala extends Actores {

    private Texture texture;
    private float vel;

    //1184 width, 768 height
    private final float widthPorcent = 3.80f;
    private final float heightPorcent = 3.90f;

    public Bala(String textura, float x, float y, float vel) {
        super();
        this.vel = vel;
        texture = new Texture(Gdx.files.internal(textura));
        sprite = new Sprite(texture);
        setSize((widthPorcent*Pantalla.width)/100,(heightPorcent*Pantalla.height)/100);
        setPosition(x, y);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        moveBy(vel * delta, 0);

        if (getX() > Pantalla.width) {
            remove();
            dispose();
        } else {
            for (Actor a : Pantalla.zombies.getChildren()) {
                Zombie zombie = (Zombie) a;
                if (sprite.getBoundingRectangle().overlaps(zombie.getRectangle())) {
                    Assets.sonidoZombie.play();
                    zombie.clear();
                    zombie.remove();
                    clear();
                    remove();
                    dispose();
                    Pantalla.contador.setZombiesMatados(Pantalla.contador.getZombiesMatados() + 1);
                }
            }
        }
    }

    private void dispose(){
        texture.dispose();
    }
}
