package com.miguelp.zombiesurvival;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

/**
 * Created by empimcf on 16/03/2018.
 */

public class MusicaService extends Service {

    private MediaPlayer reproductor;
    @Override
    public void onCreate() {

        reproductor = MediaPlayer.create(this, R.raw.jojo);
        reproductor.setLooping(true);

    }

    @Override
    public int onStartCommand(Intent intenc, int flags, int idArranque) {
        reproductor.start();

        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        reproductor.stop();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
