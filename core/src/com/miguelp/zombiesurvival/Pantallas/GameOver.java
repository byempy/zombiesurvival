package com.miguelp.zombiesurvival.Pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.miguelp.zombiesurvival.ZombieGame;

/**
 * Created by empimcf on 13/03/2018.
 */

public class GameOver extends ScreenAdapter {
    private ZombieGame game;
    private Stage stage;

    public GameOver(ZombieGame game){
        this.game = game;
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        Label.LabelStyle font = new Label.LabelStyle(new BitmapFont(), Color.WHITE);

        Table table = new Table();
        table.center();
        table.setFillParent(true);

        Label gameOverLabel = new Label("GAME OVER", font);
        gameOverLabel.setFontScale((Pantalla.width*7)/1184,(Pantalla.height*7)/768);
        Label playAgainLabel = new Label("Pulsa aquí para continuar..", font);
        playAgainLabel.setFontScale((Pantalla.width*2)/1184,(Pantalla.height*3)/768);
        playAgainLabel.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
                cambiarPantalla();
                return true;
            }
        });

        table.add(gameOverLabel).expandX();
        table.row();
        table.add(playAgainLabel).expandX().padTop((Pantalla.height*10f)/768);

        stage.addActor(table);
    }

    @Override
    public void render(float delta){
        Gdx.gl.glClearColor(0,0,0,0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    public void cambiarPantalla(){
        game.setScreen(new Pantalla(game));
        dispose();
    }
    @Override
    public void dispose(){
        stage.dispose();
    }

}