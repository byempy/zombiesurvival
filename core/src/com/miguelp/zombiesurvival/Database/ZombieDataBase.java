package com.miguelp.zombiesurvival.Database;

import java.util.ArrayList;

/**
 * Created by empimcf on 15/03/2018.
 */

public interface ZombieDataBase {

    void borrarPeorPartida();
    ArrayList<Partida> getListaPartidas();
    void guardarPartida(Partida partida);
    boolean esTop3(Partida partida);

}
