package com.miguelp.zombiesurvival;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.miguelp.zombiesurvival.Database.AndroidDataBase;
import com.miguelp.zombiesurvival.ZombieGame;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

		stopService(new Intent(getApplicationContext(), MusicaService.class));

		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		boolean FastMode = pref.getBoolean("fastMode", false);

		initialize(new ZombieGame(new AndroidDataBase(this), FastMode), config);
	}
}
