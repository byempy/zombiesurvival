package com.miguelp.zombiesurvival.Escenas;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.miguelp.zombiesurvival.Assets;
import com.miguelp.zombiesurvival.Pantallas.Pantalla;

import java.awt.Font;

/**
 * Created by empimcf on 14/03/2018.
 */

public class Contador extends Stage {
    private int zombiesMatados;
    private int zombiesTotales;
    private Label contador;

    public Contador(){
        zombiesMatados = 0;
        zombiesTotales = 0;
        contador = new Label(zombiesMatados + " / " + zombiesTotales,new Label.LabelStyle(new BitmapFont(), Color.YELLOW));
        contador.setFontScale((Pantalla.width*4)/1184,(Pantalla.height*4)/768);
        contador.setPosition((Pantalla.width*80)/1184, Pantalla.height-contador.getHeight()-((Pantalla.height*30)/768));

        Image logo = new Image(new Texture(Assets.logo));
        logo.setSize((Pantalla.width*50)/1184,(Pantalla.height*50)/768);
        logo.setPosition(contador.getX()-logo.getWidth()-((Pantalla.width*20)/1184), Pantalla.height-contador.getHeight()-(Pantalla.height*40)/768);

        addActor(contador);
        addActor(logo);
    }

    @Override
    public void draw(){
        super.draw();
        contador.setText(zombiesMatados + " / " + zombiesTotales);

    }

    public void setZombiesMatados(int zombiesMatados){
        this.zombiesMatados = zombiesMatados;
    }

    public void setZombiesTotales(int zombiesTotales){
        this.zombiesTotales = zombiesTotales;
    }

    public int getZombiesMatados(){
        return zombiesMatados;
    }

    public int getZombiesTotales(){
        return zombiesTotales;
    }


}
