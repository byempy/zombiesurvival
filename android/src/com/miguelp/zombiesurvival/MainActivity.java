package com.miguelp.zombiesurvival;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnIniciar = findViewById(R.id.btnIniciar);
        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AndroidLauncher.class);
                startActivity(i);
            }
        });

        Button btnPartidas = findViewById(R.id.btnStats);
        btnPartidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), PartidasActivity.class);
                startActivity(i);
            }
        });

        Button btnPref = findViewById(R.id.btnPrefs);
        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), PreferenciasActivity.class);
                startActivity(i);
            }
        });

        Button btnAcerca = findViewById(R.id.btnAcerca);
        btnAcerca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AcercaDeActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onStart(){
        super.onStart();
        startService(new Intent(getApplicationContext(), MusicaService.class));
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        stopService(new Intent(getApplicationContext(), MusicaService.class));
    }

}
