package com.miguelp.zombiesurvival.Actors;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.TimeUtils;
import com.miguelp.zombiesurvival.*;
import com.miguelp.zombiesurvival.Pantallas.Pantalla;

/**
 * Created by empimcf on 13/03/2018.
 */

public class Robot extends Actores {

    private float posicionY;
    private boolean subiendo;
    private boolean bajando;
    private boolean disparando;
    private float velocidad;
    private long tiempoDisparo;
    private float runFrame;
    private float cadenciaDisparo;
    private float velocidadDisparo;

    private float posicionDisparoX;
    private float posicionDisparoY;


    public Robot(String atlas, float x, float y, float velocidad, float cadencia, float velocidadDisparo) {
        super(atlas, x, y);

        this.velocidad = velocidad;
        this.cadenciaDisparo = cadencia;
        this.velocidadDisparo = velocidadDisparo;

        posicionY = y;
        runFrame = 0;
        vivo = true;

        posicionDisparoX = ((1.68f*Pantalla.width)/100);
        posicionDisparoY = ((2.60f*Pantalla.height)/100);
    }

    @Override
    public void act(float delta){
        super.act(delta);

        if(disparando){
            if(TimeUtils.millis() - tiempoDisparo >= cadenciaDisparo) {
                Assets.sonidoDisparo.play();
                Bala bala = new Bala(Assets.bala, getWidth() - posicionDisparoX, (posicionY + getHeight() / 2) - posicionDisparoY, velocidadDisparo);
                Pantalla.stage.addActor(bala);
                tiempoDisparo = TimeUtils.millis();
            }
        }

        if (estaSubiendo()) {
            posicionY += delta * velocidad;
        }

        if (estaBajando()) {
            posicionY -= delta * velocidad;
        }

        if(posicionY <= 0){
            posicionY = 0;
        }

        if(posicionY >= Pantalla.height-getHeight()){
            posicionY = Pantalla.height-getHeight();
        }

        setPosition(0, posicionY);

        if(estaBajando()|| estaSubiendo()){
            runFrame+=delta;
        }

        sprite.setRegion((TextureRegion) runAnimation.getKeyFrame(runFrame,true));

    }

    public void setVivo(boolean vivo){
        this.vivo = vivo;
    }

    public boolean estaVivo(){
        return vivo;
    }

    public void setSubiendo(boolean subiendo){this.subiendo = subiendo;}

    public boolean estaSubiendo(){return subiendo;}

    public void setBajando(boolean bajando){this.bajando = bajando;}

    public boolean estaBajando(){return bajando;}

    public void setDisparando(boolean disparando){this.disparando = disparando;}


}
