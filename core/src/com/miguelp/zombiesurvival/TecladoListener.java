package com.miguelp.zombiesurvival;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.miguelp.zombiesurvival.Pantallas.Pantalla;

/**
 * Created by empimcf on 15/03/2018.
 */

public class TecladoListener implements InputProcessor {
    @Override
    public boolean keyDown(int keycode) {
        switch(keycode){
            case Input.Keys.UP:
                Pantalla.robot.setSubiendo(true);
                break;
            case Input.Keys.DOWN:
                Pantalla.robot.setBajando(true);
                break;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch(keycode){
            case Input.Keys.UP:
                Pantalla.robot.setSubiendo(false);
                break;
            case Input.Keys.DOWN:
                Pantalla.robot.setBajando(false);
                break;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Pantalla.robot.setDisparando(true);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        Pantalla.robot.setDisparando(false);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
