package com.miguelp.zombiesurvival.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.miguelp.zombiesurvival.Pantallas.Pantalla;

/**
 * Created by empimcf on 13/03/2018.
 */

public class Zombie extends Actores {

    private Texture texture;

    public Zombie(String atlas, float x, float y) {
        super(atlas, x, y);
    }

    public Zombie(String atlas){
        super(atlas, 0, 0);
    }

    @Override
    public void act(float delta){
        super.act(delta);
        if(getRectangle().overlaps(Pantalla.robot.getRectangle())){
            Pantalla.robot.setVivo(false);
        }
        sprite.setRegion((TextureRegion) runAnimation.getKeyFrame(runFrame,true));
        runFrame+= delta;

    }

    public void iniciarMovimiento(float sec){
        float y = MathUtils.random(0, Pantalla.height-getHeight());
        setPosition(Pantalla.width, y);
        addAction(Actions.sequence(
                Actions.moveBy(-Pantalla.width-getWidth(), 0, sec),
                Actions.removeActor()
        ));
    }

}
