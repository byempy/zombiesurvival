package com.miguelp.zombiesurvival.Escenas;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.miguelp.zombiesurvival.Assets;
import com.miguelp.zombiesurvival.Pantallas.Pantalla;

/**
 * Created by empimcf on 14/03/2018.
 */

public class Controller extends Stage {

    public Controller(){

        float imagenSizeWidth = ((12.66f*Pantalla.width)/100);
        float imagenSizeHeight = ((19.53f*Pantalla.height)/100);
        float imagenPosicionY = ((2.60f*Pantalla.height)/100);
        float imagenUpPosicionX = ((1.68f*Pantalla.width)/100);
        float imagenDownPosicionX = ((16.89f*Pantalla.width)/100);
        float imagenShootPosicionX = ((3.37f*Pantalla.width)/100);

        Image upImg = new Image(new Texture(Assets.botonUp));
        upImg.setSize(imagenSizeWidth,imagenSizeHeight);
        upImg.setPosition(imagenUpPosicionX,imagenPosicionY);
        upImg.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
                Pantalla.robot.setSubiendo(true);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button){
                Pantalla.robot.setSubiendo(false);
            }
        });

        Image downImg = new Image(new Texture(Assets.botonDown));
        downImg.setSize(imagenSizeWidth,imagenSizeHeight);
        downImg.setPosition(imagenDownPosicionX,imagenPosicionY);
        downImg.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
                Pantalla.robot.setBajando(true);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button){
                Pantalla.robot.setBajando(false);
            }
        });

        Image shootImg = new Image(new Texture(Assets.bala));
        shootImg.setSize(imagenSizeWidth,imagenSizeHeight);
        shootImg.setPosition(Pantalla.width-shootImg.getWidth()-imagenShootPosicionX,imagenPosicionY);
        shootImg.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
                Pantalla.robot.setDisparando(true);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button){
                Pantalla.robot.setDisparando(false);
            }
        });

        addActor(upImg);
        addActor(downImg);
        addActor(shootImg);
    }
}
