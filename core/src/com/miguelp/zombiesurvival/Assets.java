package com.miguelp.zombiesurvival;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.miguelp.zombiesurvival.Pantallas.Pantalla;

/**
 * Created by empimcf on 13/03/2018.
 */

public class Assets {
    public static String personaje_atlasrun = "imagenes/robotrun.txt";
    public static String bala = "imagenes/bala.png";
    public static String zombie_runatlas = "imagenes/zombierun.txt";
    public static String female_zombie_runatlas = "imagenes/zombierun2.txt";
    public static String fondo = "imagenes/background.jpg";
    public static String botonUp = "imagenes/flechaup.png";
    public static String botonDown = "imagenes/flechadown.png";
    public static String logo = "imagenes/iconozombie.png";

    public static Sound sonidoDisparo;
    public static Sound sonidoZombie;
    public static Music musicaFondo;


    public static void init(){
        sonidoDisparo = Gdx.audio.newSound(Gdx.files.internal("sonidos/bala.mp3"));
        sonidoZombie = Gdx.audio.newSound(Gdx.files.internal("sonidos/zombie.wav"));
        musicaFondo = Gdx.audio.newMusic(Gdx.files.internal("sonidos/background.mp3"));
        musicaFondo.setVolume(0.5f);
        musicaFondo.setLooping(true);

    }

    public static void dispose(){
        sonidoDisparo.dispose();
        sonidoZombie.dispose();
        musicaFondo.dispose();
    }
}
