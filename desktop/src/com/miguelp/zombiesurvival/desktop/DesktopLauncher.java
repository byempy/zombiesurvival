package com.miguelp.zombiesurvival.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.miguelp.zombiesurvival.ZombieGame;
import com.miguelp.zombiesurvival.desktop.Database.DataBaseDesktop;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new ZombieGame(new DataBaseDesktop(), true), config);
	}
}
