package com.miguelp.zombiesurvival.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by empimcf on 15/03/2018.
 */

public class DataBaseHelper extends SQLiteOpenHelper{

    public DataBaseHelper(Context context) {
        super(context, "ZombiesSurvivalDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "insert into partidas(matados, total) values('%d','%d');";
        sqLiteDatabase.execSQL("create table partidas(id integer primary key autoincrement, matados integer, total integer);");
        sqLiteDatabase.execSQL(String.format(query, 0,0));
        sqLiteDatabase.execSQL(String.format(query, 0,0));
        sqLiteDatabase.execSQL(String.format(query, 0,0));
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

}
