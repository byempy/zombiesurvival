package com.miguelp.zombiesurvival.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.miguelp.zombiesurvival.Actors.Zombie;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by empimcf on 15/03/2018.
 */

public class AndroidDataBase implements ZombieDataBase {
    DataBaseHelper helper;
    SQLiteDatabase db;

    public AndroidDataBase(Context context){
        helper = new DataBaseHelper(context);
        db = helper.getWritableDatabase();
    }

    @Override
    public void borrarPeorPartida() {
        db.execSQL("delete from Partidas where id='" + getListaPartidas().get(2).getId() + "';");
    }

    @Override
    public ArrayList<Partida> getListaPartidas() {
        ArrayList<Partida> partidas = new ArrayList<Partida>();
        Cursor c = db.rawQuery("select * from partidas order by (matados - (total - matados)) desc;", null);
        if (c.moveToFirst()) {
            do {
                partidas.add(new Partida(c.getInt(0), c.getInt(1), c.getInt(2)));
            } while (c.moveToNext());
        }

        return partidas;
    }

    @Override
    public void guardarPartida(Partida partida) {
        String query = "insert into partidas(matados, total) values('%d','%d');";
        db.execSQL(String.format(query, partida.getZombiesMatados(), partida.getZombiesTotales()));
    }

    @Override
    public boolean esTop3(Partida partida) {
        for(Partida p: getListaPartidas()){
            if(p.getZombiesMatados()<partida.getZombiesMatados()){
                return true;
            }
        }
        return false;
    }

}
