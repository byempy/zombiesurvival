package com.miguelp.zombiesurvival.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.RemoveActorAction;
import com.miguelp.zombiesurvival.Pantallas.Pantalla;

/**
 * Created by empimcf on 13/03/2018.
 */

public class Actores extends Actor {
    protected TextureAtlas runAtlas;
    protected Animation runAnimation;

    protected Sprite sprite;

    protected boolean vivo;
    protected float runFrame;

    private final float widthPorcent = 8.44f;
    private final float heightPorcent = 15f;

    public Actores(){
    }

    public Actores(String atlasCorrer, float x, float y){
        super();
        sprite = new Sprite();
        runAtlas = new TextureAtlas(Gdx.files.internal(atlasCorrer));
        runAnimation =  new Animation(0.15f, runAtlas.getRegions(), Animation.PlayMode.NORMAL);
        runFrame = 0;
        vivo = true;

        setSize((widthPorcent*Pantalla.width)/100,(heightPorcent*Pantalla.height)/100);
        //setSize(100,115);
        setPosition(x, y);

    }

    @Override
    public void act(float delta){
        super.act(delta);
        setPosition(getX(), getY());
    }

    @Override
    public void draw(Batch b, float alpha){
        super.draw(b, alpha);
        sprite.draw(b);
    }

    public void setPosition(float x, float y){
        super.setPosition(x, y);
        sprite.setPosition(x, y);
    }

    @Override
    public void setSize(float width, float height){
        super.setSize(width, height);
        sprite.setSize(width, height);
    }

    public Rectangle getRectangle(){
        return sprite.getBoundingRectangle();
    }

    @Override
    public boolean remove(){
        dispose();
        return super.remove();
    }

    private void dispose()
    {
        if(runAtlas != null)
            runAtlas.dispose();
    }

}
