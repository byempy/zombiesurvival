package com.miguelp.zombiesurvival;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.miguelp.zombiesurvival.Database.AndroidDataBase;
import com.miguelp.zombiesurvival.Database.Partida;

import java.util.ArrayList;

public class PartidasActivity extends Activity {
    private final ArrayList<String> listaPartidas = new ArrayList<String>();
    AndroidDataBase db;
    ArrayList<Partida> partidas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partidas);

        db = new AndroidDataBase(this);

        ListView listaPartidas = findViewById(R.id.lstPartidas);
        partidas = db.getListaPartidas();
        ArrayAdapter<Partida> adapterLista =
                new ArrayAdapter<Partida>(this, R.layout.fila_partida,R.id.texto, partidas);
        listaPartidas.setAdapter(adapterLista);
        listaPartidas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mostrarAlertDialog(i);
            }
        });


    }

    public void mostrarAlertDialog(int i){
        String informacion = "Matados: " + partidas.get(i).getZombiesMatados() + "\nTotales: "+ partidas.get(i).getZombiesTotales();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Información de partida")
                .setMessage(informacion)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }
}
