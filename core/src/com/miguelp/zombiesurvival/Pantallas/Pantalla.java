package com.miguelp.zombiesurvival.Pantallas;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Timer;
import com.miguelp.zombiesurvival.Actors.Zombie;
import com.miguelp.zombiesurvival.Assets;
import com.miguelp.zombiesurvival.Actors.Robot;
import com.miguelp.zombiesurvival.Database.Partida;
import com.miguelp.zombiesurvival.Escenas.Contador;
import com.miguelp.zombiesurvival.Escenas.Controller;
import com.miguelp.zombiesurvival.TecladoListener;
import com.miguelp.zombiesurvival.ZombieGame;

/**
 * Created by empimcf on 13/03/2018.
 */

public class Pantalla extends ScreenAdapter{
    private ZombieGame game;
    public static Robot robot;
    public static Stage stage;
    private Texture background;
    public static Group zombies;
    public Timer.Task generadorZombies;
    private Controller controller;
    public static Contador contador;

    public static int width;
    public static int height;

    private  float velocidadRobot;
    private float velocidadZombies;
    private float velocidadGeneradorZombies;
    private float cadenciaDisparo;
    private float velocidadDisparo;

    public Pantalla(ZombieGame game){
        this.game = game;
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();

        if(game.FastMode){
            velocidadRobot = 400;
            velocidadZombies = 2f;
            velocidadGeneradorZombies = 0.5f;
            cadenciaDisparo = 300;
            velocidadDisparo = 1100;
        }else{
            velocidadRobot = 300;
            velocidadZombies = 3f;
            velocidadGeneradorZombies = 1.0f;
            cadenciaDisparo = 500;
            velocidadDisparo = 700;
        }

        velocidadRobot = (Pantalla.height*velocidadRobot)/768;
        velocidadDisparo = (Pantalla.width*velocidadDisparo)/1184;
        stage = new Stage();
        contador = new Contador();
        robot = new Robot(Assets.personaje_atlasrun, 0, height/2,velocidadRobot, cadenciaDisparo, velocidadDisparo);
        background = new Texture(Gdx.files.internal(Assets.fondo));

        stage.addActor(robot);

        zombies = new Group();
        stage.addActor(zombies);

        generadorZombies = new Timer.Task(){
            @Override
            public void run() {
                positionZombies();
            }

        };

        Timer.schedule(generadorZombies, 0.5f, velocidadGeneradorZombies);

        Assets.musicaFondo.play();

        if(Gdx.app.getType()== Application.ApplicationType.Android) {
            controller = new Controller();
            Gdx.input.setInputProcessor(controller);
        }else{
            Gdx.input.setInputProcessor(new TecladoListener());
        }

    }

    private void positionZombies(){
        Zombie zombie = null;
        float velocidad;
        float tipo = MathUtils.random(0, 1);
        if(tipo >= 0 && tipo <0.50f){
            zombie = new Zombie(Assets.zombie_runatlas);
            velocidad = velocidadZombies;
        }else{
            zombie = new Zombie(Assets.female_zombie_runatlas);
            velocidad = velocidadZombies-0.5f;
        }

        zombie.iniciarMovimiento(velocidad);
        zombies.addActor(zombie);
        contador.setZombiesTotales(contador.getZombiesTotales()+1);
    }

    @Override
    public void render(float delta){
        Gdx.gl.glClearColor(0,0,0,0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.getBatch().begin();
        stage.getBatch().draw(background, 0, 0, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        stage.getBatch().end();

        stage.act(delta);
        stage.draw();

        if(Gdx.app.getType()== Application.ApplicationType.Android)
            controller.draw();

        contador.draw();

        if(!robot.estaVivo()){
            Partida partida = new Partida(contador.getZombiesMatados(), contador.getZombiesTotales());
            if(game.db.esTop3(partida)){
                game.db.borrarPeorPartida();
                game.db.guardarPartida(partida);
            }
            Assets.musicaFondo.stop();
            game.setScreen(new GameOver(game));
            generadorZombies.cancel();
            robot.remove();
            dispose();
        }
    }

    @Override
    public void dispose(){
        super.dispose();
        stage.dispose();
        background.dispose();
        contador.dispose();
        if(Gdx.app.getType()== Application.ApplicationType.Android)
            controller.dispose();

    }
}
