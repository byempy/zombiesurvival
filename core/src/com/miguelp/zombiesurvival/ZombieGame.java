package com.miguelp.zombiesurvival;

import com.badlogic.gdx.Game;
import com.miguelp.zombiesurvival.Database.ZombieDataBase;
import com.miguelp.zombiesurvival.Pantallas.Pantalla;


public class ZombieGame extends Game {
	public ZombieDataBase db;
	public boolean FastMode;

	public ZombieGame(ZombieDataBase db, boolean FastMode){
		this.FastMode = FastMode;
		this.db = db;
	}

	@Override
	public void create() {
		Assets.init();
		setScreen(new Pantalla(this));
	}

	@Override
	public void dispose(){
		super.dispose();
		Assets.dispose();
	}
}
